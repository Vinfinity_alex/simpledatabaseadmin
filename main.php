<?php

session_start(); 
$server = $_SESSION['server'];
$user = $_SESSION['user'];
$pass = $_SESSION['pass'];
$db = $_SESSION['db'];

$connection = mysqli_connect($server, $user, $pass, $db);

	if($connection == false)
	{	
		echo 'Connection error <br>';
		echo mysqli_connct_error();
	}

//Query
if (isset($_POST['btnActionQuery']))
{
    $dbname = $_POST['query'];
    mysqli_query($connection, $dbname);
}

//DB Create
if (isset($_POST['btnActionDBCreate']))
{
    $dbname = $_POST['database_name'];
    mysqli_query($connection, 'CREATE DATABASE '. $dbname);
}

//DB Drop
if (isset($_POST['btnActionDBDrop']))
{
    $dbname = $_POST['database_name'];
    mysqli_query($connection, 'DROP DATABASE '. $dbname);
}

//Create table
if (isset($_POST['CreateTable']))
{
    $tbname = $_POST['table_name'];
    $dbname = $_POST['db_name'];
    mysqli_select_db($connection, $dbname);
    mysqli_query($connection, 'CREATE TABLE '. $tbname . "()");
}

function db_list()
{		
	$sql = "SHOW DATABASES";

	global $connection;
	global $server;
	global $user;
	global $pass;
	global $db;

	if (!($result = mysqli_query( $connection, $sql))) 
	{
        printf("Error: %s\n", mysqli_error($connection));
    }

	while( $row = mysqli_fetch_row( $result ) )
	{
        if (($row[0]!="information_schema") && ($row[0]!="mysql")) 
        {
            echo "<form method='POST' action = '/sql/showdb.php'>";
            echo "<input type='hidden' name='server' value=" . $server . ">" .
				"<input type='hidden' name='user' value=" . $user . ">" .
				"<input type='hidden' name='pass' value=" . $pass . ">" .
				"<input type='hidden' name='db' value=" . $row[0] . ">"
				;
            echo "<input type = 'submit' value = " . $row[0] . ">"
			."</form>";
        }
    }
}

?>

<table >
  <tr>
    <td width = 200>
    	<h3>Menu</h3>
    	<form method="GET" action = "/sql/cppsql.php">
			<input type = "submit" value = "Sql query" name='cppsql'>
		</form>
		<form method="POST" action = "/sql/createdatabase.php">
			<input type = "submit" value = "Create database">
		</form>
		<form method="POST" action = "/sql/dropdatabase.php">
			<input type = "submit" value = "Drop database">
		</form>
		<form method="POST" action = "/sql/createtable.php">
			<input type = "submit" value = "Create table">
		</form>
		<form action = "/sql/index.php">
			<input type = "submit" value = "Exit">
		</form>
    </td>
    <td>
    	<h3>Databases</h3>
    	<?php
    		echo db_list();
    	?>
    </td>
  </tr>
</table>