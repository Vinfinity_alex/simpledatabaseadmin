#include <iostream>
#include <cstdlib>
#include <mysql.h>
#include <cstring>
#include <fstream>

using namespace std;

MYSQL *connection, mysql;
MYSQL_RES *res;
MYSQL_ROW row;
char *query;
int query_state = 0;

void replace(char *str, char c1, char c2)
{
    char *p = str;
    while (p = strchr(p, c1))
    {
        *p++ = c2;
    }
}

void die(void){
   cout<<"%s\n"<<mysql_error(&mysql)<<endl;
   exit(0);
}

void SendQuery(char *query, char *server, char *user, char *password, char *db, int socket, char *unix_socket, long clientflag){
    unsigned int i = 0;
    if (!mysql_init (&mysql)) abort ();
    if (!(mysql_real_connect(&mysql, server, user, password, db, socket, unix_socket, clientflag)))
       die();
    if (mysql_select_db(&mysql, db))
       die();
    if (mysql_query(&mysql, query))
       die();
    if (!(res = mysql_store_result(&mysql)))
       die();
    while(row = mysql_fetch_row(res)) {
        cout<<"<br>";
        for (i = 0 ; i < mysql_num_fields(res); i++)
           cout<<row[i]<<"  ";
        //cout<<"</tr>";
     }
     if (!mysql_eof(res))
        die ();
     mysql_free_result(res);
     mysql_close(&mysql);
}


int main()
{
    char* st=getenv("QUERY_STRING");//get
    char* m="Content-Type:html\n\n";
    int col;
    int row;

    if (st)
    {
        cout<<m;
        cout<<"<html><head></head><body><br>"<<endl;
        cout<<"<br>";
        char* part = 0;
        const int Size=10;
        char* x[Size];
        part=strtok(st,"&=");
        int i=0;
        while(part)
        {
            x[i]=part;
            i++;
            part=strtok(0,"&=");
        }

        //query execute
        query = x[i-1];
        replace(query, '+', ' ');
        SendQuery(query, x[1], x[3], "", x[6], 3306, 0, 0);

        cout<<"<?php Location: /sql/main.php ?>";
    }

return 0;
}
